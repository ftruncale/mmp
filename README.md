# Musically Musical Player
Based on "qTunes" by George Wolberg, assigned as part of CSC322.

Requires QT4 to compile, compile with Qt Creator.

Documentation available in ``doxygen/index.html``
