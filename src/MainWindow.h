// ======================================================================
// MainWindow.cpp - Main Window widget
//
// Lazy Asian Development Syndicate - Musically Musical Player
// ======================================================================

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QtWidgets>
#include <QOpenGLWidget>
#include <QMap>
#include "coverflow.h"
#include "spectrum.h"

///////////////////////////////////////////////////////////////////////////////
///
/// \class MainWindow
/// \brief MainWindow class
///
/// The main window consists of dock widgets, which contain widgets
/// that are used to display and control media from a selected folder.
///
///////////////////////////////////////////////////////////////////////////////

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    //! Constructor.
    MainWindow(QString);

    //! Destructor.
    ~MainWindow();

public slots:
    // slots
    void s_load  ();
    void s_panel1(QListWidgetItem*);
    void s_panel2(QListWidgetItem*);
    void s_panel3(QListWidgetItem*);
    void s_about ();
    void s_ml_add_play(QTableWidgetItem*); //Public.
    void s_header_adjust(int);


    // Layout Controls
    void s_toggleDocks();

    //Player slots
    void s_pause();
    void s_stop();
    void s_play();
    void s_mute();
    void s_previous();
    void s_next();
    void s_playmode();
    void changeVolume(int);
    void s_player_status(QMediaPlayer::State);
    void s_progress_slider_change(int);
    void s_progress_slider_loc_change(qint64);

    void s_playback_speed(int);

    void playerStatusUpdate(qint64);
    void s_duration(qint64);
    void s_extract_art(QTableWidgetItem*);
    void s_playlist_spec(QTableWidgetItem*);

    void s_media_changed();

private:
    void createActions();
    void createMenus  ();
    void createWidgets();
    void createLayouts();
    void initLists	  ();
    void redrawLists  (QListWidgetItem *, int);
    void traverseDirs (QString);

    void listFix();

    int m_lastsort = -1;
    bool m_sortorder = 0;
    int m_last_playing = 0;

    // actions
    QAction		*m_loadAction;
    QAction		*m_quitAction;
    QAction		*m_aboutAction;
    QAction     *m_toggleDockAction;

    // menus
    QMenu		*m_fileMenu;
    QMenu		*m_widgetMenu;
    QMenu		*m_helpMenu;

    // widgets
    Coverflow *coverflow;
    spectrum *spectrum_widget;
    QWidget *p_controls;
    QWidget *m_mainWidget;
    QLabel		*m_labelArt;
    QLabel		*m_label[3];
    QComboBox   *m_playback_speed;

    QListWidget 	*m_panel[3];



    QTableWidget	*m_table;
    QPushButton	*m_stop;

    //Dockable junk
    QDockWidget *coverflow_dock;
    QDockWidget *p_controls_dock;
    QDockWidget *spectrum_dock;
    QDockWidget *m_table_dock;
    QDockWidget *m_panel_dock;
    QDockWidget *m_playlist_dock;
    QDockWidget *m_labelArt_dock;


    bool m_cdisabled = 0;

    //Playlist section
    QTableWidget *m_playlist_view;

    //Other widgets
    QImage *m_curr_album_art;
    QImage *m_default_album_art;
    QPixmap *m_tempart;


    //ENUM'd stuff
    enum enum_controls {
        PREVIOUS,
        PLAY, STOP,
        NEXT,SHUFFLE,
        ENUM_CONTROLS_END
    };

    enum enum_aud_controls {
        MUTE, ENUM_AUD_CONTROLS_END
    };


    //Player specific
    QMediaPlayer* mediaplayer;
    QMediaPlaylist* playlist;
    QPushButton *player_controls[ENUM_CONTROLS_END];

    QSlider *m_progress_slider;
    QSlider *m_volume_slider;
    QLabel *m_playtime;

    int m_progress_value = 0;
    int m_track_duration = 0;
    QPushButton *m_aud_controls[ENUM_AUD_CONTROLS_END];

    //Utility functions
    //QListWidgetItem *
    void load_default_album_art(); //Runs once

    // string lists
    QString		   m_directory;
    QStringList	   m_listGenre;
    QStringList	   m_listArtist;
    QStringList	   m_listAlbum;
    QList<QStringList> m_listSongs;

    QString convertTime(qint64);

};

#endif // MAINWINDOW_H
