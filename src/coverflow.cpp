#include <cstdlib>
#include <GL/glu.h>
#include <QApplication>
#include <QtWidgets>
#include <QGLWidget>
#include <QTimer>
#include <cmath>
#include "coverflow.h"
#include <QOpenGLWidget>
using namespace std;


Coverflow::Coverflow(QWidget *parent):
    /** Widget initialized.
     */
    QOpenGLWidget(parent){
    initializeGL();
    setFocusPolicy(Qt::StrongFocus);
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()),this,SLOT(update()));
    m_timer->start(25);

}


void Coverflow::update() {
    paintGL();
}


void Coverflow::paintGL(){
    display();
}

void Coverflow::initializeGL(){
    glEnable(GL_DEPTH_TEST);
    glClearColor(0, 0, 0, 0);
}

void Coverflow::setNumberOfBoxes(int num){
    number_of_boxes = num;
}

int Coverflow::NumberOfBoxes(){
    return number_of_boxes;
}

void Coverflow::perspective(GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar)
{
    GLdouble xmin, xmax, ymin, ymax;

    ymax = zNear * tan( fovy * M_PI / 360.0 );
    ymin = -ymax;
    xmin = ymin * aspect;
    xmax = ymax * aspect;

    glFrustum( xmin, xmax, ymin, ymax, zNear, zFar );
}

void Coverflow::loadTexture(int i){
    GLuint* temp = new GLuint;
    QImage tempConversion(QGLWidget::convertToGLFormat(ArtList[i]));
    glGenTextures(1, temp);
    glBindTexture(GL_TEXTURE_2D, *temp);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tempConversion.width(), tempConversion.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tempConversion.bits());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    textures.push_back(*temp);
}


void Coverflow::resizeGL(int w, int h)
{
    float ratio =1.0*w/h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    perspective(22.5,ratio,1,1000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Coverflow::drawbox(int index){
    /** Draws a texture mapped cover and its reflection
     */
    if (textures.size() < index+1){
        loadTexture(index);
    }

    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_FLAT);
    glBindTexture(GL_TEXTURE_2D, textures[index]);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor3f(1, 1, 1);

    if (index < middleshift){ //Condition to flip the textures
        glBegin(GL_POLYGON);
        glTexCoord2f(1, 0); glVertex3f(-0.5,-0.5,0);
        glTexCoord2f(1, 1); glVertex3f(-0.5, 0.5,0);
        glTexCoord2f(0, 1); glVertex3f( 0.5, 0.5,0);
        glTexCoord2f(0, 0); glVertex3f( 0.5,-0.5,0);
        glEnd();
    }
    else { //Normal texture mapping
        glBegin(GL_POLYGON);
        glTexCoord2f(0, 0); glVertex3f(-0.5,-0.5,0);
        glTexCoord2f(0, 1); glVertex3f(-0.5, 0.5,0);
        glTexCoord2f(1, 1); glVertex3f( 0.5, 0.5,0);
        glTexCoord2f(1, 0); glVertex3f( 0.5,-0.5,0);
        glEnd();
    }

}

void Coverflow::display()
{
    /** Displays the coverflow depending on the amount of music
     */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    for (int i = 0; i < number_of_boxes; i++){
        glPushMatrix();
        //if at the center, push the album closer to the viewer
        if(i==middleshift)glTranslatef(i, 0, -4);
                        else glTranslatef(i, 0, -5);

            glTranslatef(xloc, 0.0f, 0.0f);//used for animation and displacement
            glRotatef(-90, 0, 1, 0);

            //if i is the center panel, rotate it to face us
            if(i==middleshift){
                            if(middleshift%2==0)glRotatef(90, 0, 1, 0);//if i don't do this the ones on the side rotate as well
                            if(middleshift%4==2||middleshift%4==3)glRotatef(180, 0, 1, 0);//sometimes the texture is flipped. This fixes it
                            glRotatef(angle/speed, 0, 1, 0);
               }

            //these deal with the animation of the panels leaving the middle index
            if(i==middleshift-1&&leaving){
                            if(middleshift%2==1)glRotatef(90, 0, 1, 0);
                            if((middleshift-1)%4==0||(middleshift-1)%4==1)glRotatef(180, 0, 1, 0);
                            glRotatef(angle/speed, 0, 1, 0);
             }


            else if(i==middleshift+1&&!leaving){
                if(middleshift%2==1)glRotatef(90, 0, 1, 0);
                if((middleshift+1)%4==2||(middleshift+1)%4==3)glRotatef(180, 0, 1, 0);
                glRotatef(angle/speed, 0, 1, 0);

            }


            drawbox(i);

            glPopMatrix();

            //reflections

                      glPushMatrix();

                      if(i==middleshift)
                          glTranslatef(i, 0, -4);
                      else
                          glTranslatef(i, 0, -5);

                          glTranslatef(0, -1, 0);
                          glTranslatef(xloc, 0.0f, 0.0f);
                          glRotatef(-90, 0, 1, 0);
                          glRotatef(180, 1, 0, 0);
                          if(i==middleshift){
                              if(middleshift%2==0)glRotatef(90, 0, 1, 0);
                              if(middleshift%2==0)glRotatef(180, 0, 1, 0);
                              glRotatef(-angle/speed, 0, 1, 0);
                          }
                          if(i==middleshift-1&&leaving){
                                          if(middleshift%2==1)glRotatef(90, 0, 1, 0);
                                          if((middleshift-1)%4==0||(middleshift-1)%4==1)glRotatef(180, 0, 1, 0);
                                          glRotatef(-angle/speed, 0, 1, 0);
                           }
                          else if(i==middleshift+1&&!leaving){
                              if(middleshift%2==1)glRotatef(90, 0, 1, 0);
                              if((middleshift+1)%4==2||(middleshift+1)%4==3)glRotatef(180, 0, 1, 0);
                              glRotatef(-angle/speed, 0, 1, 0);

                          }


                          drawbox(i);
                          glPopMatrix();
    }
}

//xloc increases by 1/250 250 times
//angle decreases by 90/250 250 times
//the timer stops calling xplus/xminus after 250 repetitions
void Coverflow::xplus(){
    /** Enables the coverflow to move to the right.
     */
    xloc+= 1/speed;
    leaving=0;
    counter++;
    if (counter<=speed){
        angle-=90.0000000;
    }

    if (counter > speed){ a_timer->stop(); allgood = 1; return; }
}

void Coverflow::xminus(){
    /** Enables the coverflow to move to the left.
     */
    xloc -= 1/speed;
    leaving=1;
    counter++;
    if(counter<=speed){
        angle+=90.0000000;
    }

    if (counter > speed){ b_timer->stop(); allgood = 1; return; }
}

void Coverflow::move_to_index(int index){
    /** Centers the album of the given index.
     */
    int difference = index-middleshift;//find the difference between the old and new indeces. only works for non-0 numbers
    if (difference < 0){
        move_left = abs(difference);
        magic_timer = new QTimer;
        connect(magic_timer,SIGNAL(timeout()), this, SLOT(magic()));
        magic_timer->start(1);


    }
    else if (difference > 0) {
        move_right=difference;
        magic_timer = new QTimer;
        connect(magic_timer,SIGNAL(timeout()), this, SLOT(magic()));
        magic_timer->start(1);

    }
}


void Coverflow::magic(){
    /** Used as a slot function for move_to_index to fix movement
     */
    if (allgood && move_left > 0){
        move_left--;
        left_action_internal();
    }

    else if (allgood && move_right > 0){
        move_right--;
        right_action_internal();
    }

    if (move_left == 0 && move_right == 0)
        magic_timer->stop();

}

//allgood makes sure that you cannot input new actions in the middle of animation.
//left and right action internalstart a timer that calls xplus and xminus while counter<speed;
void Coverflow::left_action_internal(){
    /** Moves to the left without breaking the animation
     */
    if (allgood){
        if(middleshift>0){
            a_timer=new QTimer;
            connect(a_timer, SIGNAL(timeout()),this,SLOT(xplus()));
            allgood = 0;
            a_timer->start(1);
            middleshift-=1;
            counter=0;
            }
    }
}

void Coverflow::right_action_internal(){
    /** Moves to the right without breaking the animation
     */
    if (allgood){
        if(middleshift<number_of_boxes-1){
            b_timer=new QTimer;
            connect(b_timer, SIGNAL(timeout()),this,SLOT(xminus()));
            allgood = 0;
            b_timer->start(1);
            middleshift+=1;
            counter=0;
        }
    }
}

void Coverflow::keyPressEvent(QKeyEvent *event){
    /** Allows the user to intercept left and right arrow keys to move
     */
    switch ( event->key() ) {
    case Qt::Key_Left:
        left_action_internal();
        break;
    case Qt::Key_Right:
        right_action_internal();
        break;
    default:
        event->ignore();
        break;
    }
}


