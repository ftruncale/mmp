#ifndef SPECTRUM_H
#define SPECTRUM_H

#include <QOpenGLWidget>
#include <QObject>
#include <QImage>
#include <QList>
#include <QTimer>

///////////////////////////////////////////////////////////////////////////////
///
/// \class spectrum
/// \brief spectrum class.
///
/// A real-time analyzer that measures that measures frequency bands.
/// The frequency is displayed using oscillating bars.
///
///////////////////////////////////////////////////////////////////////////////

class spectrum : public QOpenGLWidget
{
    Q_OBJECT

public:
    spectrum(QWidget *parent = 0);
    void stopFeed();
    void startFeed();
    void pauseFeed();
public slots:
       void randomize();
       void adjust();
       void internalWindDown();

private:
        QTimer* m_timer;
        QTimer* a_timer;
        QTimer* b_timer;
        QTimer* c_timer;
        void drawbox(int);
        float sizes[11];
        float oldsizes[11];

        void update();
        void display();

        bool winded_down = 1;
        bool firstrun=1;


protected:
        void resizeGL();
        void paintGL();
        void initializeGL();


    };
#endif // SPECTRUM_H
